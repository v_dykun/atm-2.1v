import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Scanner;

public class UserServiceTest {

    @Test
    public void testMenu() throws Exception {
        UserService userService = new UserService();
        Method method = userService.getClass().getDeclaredMethod("userMenu");
        method.setAccessible(true);
        method.invoke(userService);
    }
}
