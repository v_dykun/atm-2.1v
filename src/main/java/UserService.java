import com.sun.media.jfxmedia.logging.Logger;
import entity.User;
import json.AbstractJson;
import json.AdminJson;
import json.UserJson;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UserService {
    private AdminJson adminJSON = new AdminJson();
    private UserJson userJSON = new UserJson();
    private Integer atmBalance = adminJSON.atmBalance;          // take a balance ATM with admin.json
    private User user;                                          // variable for future use

    // Check login and password. Remove user with List users in json.UserJson.class
    public void enterUser() throws IOException {
        int a = 0;                                                // variable for user search
        userJSON.firstMethod();                                 // read user.json for search user

        System.out.println("Enter entity.User Login:");
        String login = new Scanner(System.in).nextLine();

        for (int i = 0; i < userJSON.users.size(); i++) {
            User userLocal = userJSON.users.get(i);
            if (userLocal.getLogin().equals(login)) {
                a++;                                                // means that the user with such a name is in the database
                System.out.println("Enter Password For this entity.User:");
                try {
                    Integer password = new Scanner(System.in).nextInt();
                    if (userLocal.getPassword().equals(password)) {
                        user = userJSON.users.get(i);             // assign the variable found to the user
                        userJSON.users.remove(i);               // remove user from Users List in userJSON.class
                        userMenu();
                        break;
                    } else {
                        System.out.println("The password does not match the login " + login);
                        exitUser();
                    }
                } catch (InputMismatchException e) {          // you can enter only numbers
                    System.out.println("Don't enter latter!");
                    System.out.println();
                    enterUser();
                }
            }
        }
//if a = 0, then such user is not in the base
        if (a == 0) {
            System.out.println("This user is not in the database.");
            System.out.println("Contact the bank and become our client");
            System.out.println();
            Main.startMenu();
        }


    }

    // start users menu after check login and password. user = user with user.json
    private void userMenu() throws IOException {
        try {
            System.out.println();
            System.out.println("You are logged in as an " + user.getLogin() + ", select the following action:");
            System.out.println("1) You balance " + "\n" + "2) Put Cash " + "\n" + "3) Take Cash" + "\n" + "4) Exit user mode");
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    user.balance();
                    endMenu();
                    break;
                case 2:
                    putCash();
                    endMenu();
                    break;
                case 3:
                    takeCash();
                    endMenu();
                    break;
                case 4:
                    exitUser();
                    break;
                default:
                    System.out.println("This option does not exist!");
                    break;
            }
        } catch (InputMismatchException e) {                   // you can enter only numbers
            System.out.println("Don't enter latter!");
            System.out.println();
            userMenu();
        }
    }

    private void putCash() {
        try {
            System.out.println("Specify the amount you want to put: ");
            int cash = new Scanner(System.in).nextInt();
            if (cash > 0) {                                       // entered number should be greater than 0
                user.setBalance(user.getBalance() + cash);
                atmBalance = atmBalance + cash;
                user.balance();
            } else {
                System.out.println("The entered amount can not be less than 0");
                System.out.println();
                putCash();
            }
        } catch (InputMismatchException e) {                    // you can enter only numbers
            System.out.println("Don't enter latter!");
            System.out.println();
            putCash();
        }

    }

    private void takeCash() {
        try {
            System.out.println("Specify the amount you want to take: ");
            int cash = new Scanner(System.in).nextInt();
            if (cash > 0) {                                       // entered number should be greater than 0
                if (user.getBalance() > cash) {                   // checking for the presence of the amount in the user
                    if (atmBalance > cash) {                      // checking for the presence of the amount in the user
                        user.setBalance(user.getBalance() - cash);// we reduce the balance of the user and write it down
                        atmBalance = atmBalance - cash;             // we reduce the balance of the ATM
                        user.balance();
                    } else {
                        System.out.println("ATM is not enough cash.");
                        System.out.println("At the ATM account there is: " + atmBalance);
                        System.out.println("The nearest ATM is located on the street Zelena, 150");
                        System.out.println();
                    }
                } else {
                    System.out.println("You do not have enough funds in your account!");
                    System.out.println("Please, refill your account!");
                    System.out.println();
                }
            } else {
                System.out.println("The entered amount can not be less than 0");
                System.out.println();
                takeCash();
            }
        } catch (InputMismatchException e) {                    // you can enter only numbers
            System.out.println("Don't enter latter!");
            System.out.println();
            takeCash();
        }
    }

    private void endMenu() throws IOException {
        try {
            System.out.println("You want to leave entity.User mode:");
            System.out.println("1) NO" + "\n" + "2) YES");
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    userMenu();
                    break;
                case 2:
                    exitUser();
                    break;
                default:
                    System.out.println("This option does not exist!");
                    endMenu();
                    break;
            }
        } catch (InputMismatchException e) {                    // you can enter only numbers
            System.out.println("Don't enter latter!");
            System.out.println();
            endMenu();
        }
    }

    // put user after changes in List users in json.UserJson.class and exit with start users menu.
    private void exitUser() throws IOException {
        adminJSON.admin.setBalance(atmBalance);                 // write ATM balance for entity.Admin
        AdminJson.writeJson(AdminJson.LINK, adminJSON.admin);              // write entity.Admin for admin.json
        userJSON.addUser(user);                                 // add user to Users List in userJSON.class
        // re-write user.json
        AbstractJson.writeJson(UserJson.LINK, UserJson.users);
        System.out.println("Thank you for using ATM");
        System.out.println();
        Main.startMenu();
    }
}
