package entity;

public abstract class AbstractUser {

    private String login;
    private Integer password;
    private Integer balance;

    public Integer getPassword() {
        return password;
    }

    public void setPassword(Integer password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    abstract boolean checkLogin(String login);
    abstract boolean checkPassword(Integer password);
    abstract void balance();
}
