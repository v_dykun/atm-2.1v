import entity.Admin;
import json.AbstractJson;
import json.AdminJson;
import json.UserJson;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AdminService {

    private Admin admin;
    private AdminJson adminJSON = new AdminJson();

 // Check admin login and password (with admin.json).
    public void enterAdmin() throws IOException {
        try {
            adminJSON.firstMethod();                                    // open admin.json
            admin = adminJSON.admin;
            System.out.println("Enter entity.Admin Login:");
            String login = new Scanner(System.in).nextLine();
            if (admin.checkLogin(login)){
                System.out.println("Enter Password For entity.Admin:");
                int password = new Scanner(System.in).nextInt();
                if (admin.checkPassword(password)) {
                    adminMenu();

                } else { System.out.println("The password does not match the login entity.Admin");
                    exitAdmin();
                }
            } else {
                System.out.println("This login does not have administrator rights!");
                System.out.println("You may login as a user, or try again.");
                System.out.println();
                exitAdmin();
            }
        }   catch (InputMismatchException e) {
            System.out.println("Don't enter latter!");
            System.out.println();
            enterAdmin();
        }
    }

    private void adminMenu() throws IOException {
        System.out.println("You are logged as an admin, select the following action:");
        System.out.println("1) ATM balance " + "\n" + "2) Refill ATM balance " + "\n" + "3) Create user"  + "\n" + "4) Exit admin mode");
        try {
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    admin.balance();
                    endMenu();
                    break;
                case 2:
                    admin.refillAtmBalance();
                    endMenu();
                    break;
                case 3:
                    checkLoginAndCreateUser();
                    endMenu();
                    break;
                case 4:
                    exitAdmin();
                    break;
                default:
                    System.out.println("This option does not exist!");
                    System.out.println();
                    adminMenu();
                    break;
            }
        }  catch (InputMismatchException e) {
            System.out.println("Don't enter latter!");
            System.out.println();
            adminMenu();
        }
    }

    private void endMenu() throws IOException {
        System.out.println("You want to leave entity.Admin mode:");
        System.out.println("1) NO" + "\n" + "2) YES");
        try {
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    adminMenu();
                    break;
                case 2:
                    exitAdmin();
                    break;
                default:
                    System.out.println("This option does not exist!");
                    System.out.println();
                    endMenu();
                    break;
            }
        }   catch (InputMismatchException e) {
            System.out.println("Don't enter latter!");
            System.out.println();
            endMenu();
        }
    }
// exit admin mode and write admin.json
    private void exitAdmin() throws IOException {
        AdminJson.writeJson(AdminJson.LINK, admin);
        System.out.println("Thank you for using admin mode!");
        System.out.println();
        Main.startMenu();
    }

    private void checkLoginAndCreateUser() throws IOException {
        UserJson userJSON = new UserJson();                             // open user.json
        userJSON.firstMethod();
        System.out.println("Enter user Login:");
        String login = new Scanner(System.in).nextLine();

        int a=0;
        for (int i=0;i<userJSON.users.size();i++) {                     // check users by name
            if (userJSON.users.get(i).getLogin().equals(login)) {       // if user with such a name already exists
                a++;                                                    // in user.json - a++
                break;
            }
        }

        if (a==0) {                                                     // if the user with such a name is not available yet,
            userJSON.addUser(admin.createUser(login));                  // admin creates a user and add him in user.json
            AbstractJson.writeJson(UserJson.LINK, UserJson.users);
            System.out.println();
            } else {
                System.out.println("Such a user already exists !! Try again !!");
                System.out.println();
                checkLoginAndCreateUser();
                }
    }
}
