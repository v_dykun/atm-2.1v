package json;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Admin;


import java.io.File;
import java.io.IOException;

public class AdminJson extends AbstractJson {
    public static final String LINK = "src/main/resources/admin.json";            // path, where will be stored admin.json
    private static File file = new File(LINK);
    public static Admin admin;
    public Integer atmBalance = openAtmBalansceForUser();                       // this variable use in UserService-method takeCash

    public void firstMethod()  throws IOException {
        if (file.exists() && !file.isDirectory()) {
            admin = readAdminJSON();
        } else {
            Admin admin = new Admin();
            admin.setLogin("admin");
            admin.setPassword(1234);
            admin.setBalance(100000);
            AdminJson.writeJson(AdminJson.LINK, admin);
            firstMethod();
        }
    }

    public Integer openAtmBalansceForUser() {
            admin = readAdminJSON();
            return admin.getBalance();
    }

    private static Admin readAdminJSON() {
        ObjectMapper mapper = new ObjectMapper();
        try {
           admin = mapper.readValue(new File(LINK), Admin.class);
        } catch (JsonMappingException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admin;
    }

}
