package json;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;


import java.io.File;
import java.io.IOException;
import java.util.List;

public class UserJson extends AbstractJson {

    public static final String LINK = "src/main/resources/user.json";
//    public static final String LINK = "src" + File.pathSeparator + "main" + File.pathSeparator + "resources" + File.pathSeparator + "admin.json";

    private static File file = new File(LINK);
    public static List<User> users;

    public void addUser(User user) {
        users.add(user);
    }

// check whether such file exists
    public void firstMethod()  throws IOException {
        if (file.exists()&&!file.isDirectory()) {
            users = readUserJSON();                                 // users = array users with user.json
        } else {
            AbstractJson.writeJson(UserJson.LINK, UserJson.users);
            User user = new User();
            user.setLogin("user");
            user.setPassword(0000);
            addUser(user);
            firstMethod();
        }
    }

    private static List<User> readUserJSON() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            users = mapper.readValue(new File(LINK), new TypeReference<List<User>>(){});

        } catch (JsonMappingException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
}
